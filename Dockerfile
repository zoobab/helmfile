FROM alpine:3.17
#RUN wget http://archive.debian.org/debian/pool/main/o/openssl/libssl1.1_1.1.0l-1~deb9u1_amd64.deb
#RUN wget http://archive.debian.org/debian/pool/main/o/openssl/openssl_1.1.0l-1~deb9u1_amd64.deb
#RUN wget http://archive.debian.org/debian/pool/main/c/ca-certificates/ca-certificates_20200601~deb9u1_all.deb
#RUN wget http://security.debian.org/debian-security/pool/updates/main/o/openssl/libssl1.1_1.1.1n-0+deb10u5_amd64.deb
#RUN wget http://security.debian.org/debian-security/pool/updates/main/o/openssl/openssl_1.1.1n-0+deb10u5_amd64.deb
#RUN wget http://deb.debian.org/debian-security/pool/updates/main/o/openssl/libssl-dev_1.1.1n-0+deb10u5_amd64.deb
#RUN wget http://snapshot.debian.org/archive/debian/20190326T120000Z/pool/main/o/openssl/libssl1.1_1.1.0j-1~deb9u1_amd64.deb
#RUN wget http://snapshot.debian.org/archive/debian/20190326T120000Z/pool/main/o/openssl/openssl_1.1.0j-1~deb9u1_amd64.deb
RUN wget http://archive.debian.org/debian/pool/main/p/patch/patch_2.7.5-1+deb9u2_amd64.deb
RUN wget https://github.com/jorge-lip/udocker-builds/raw/master/tarballs/udocker-englib-1.2.10.tar.gz
